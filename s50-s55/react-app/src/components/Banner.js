// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'
// import Button from 'react-bootstrap/Button'

// import React from 'react';

// import {Row, Col, Button} from 'react-bootstrap';

// export default function Banner ({ title, subtitle, buttonText, buttonVariant }) {
//   return (
//     <Row>
//       <Col className="p-5">
//         <h1>{title}</h1>
//         <p>{subtitle}</p>
//         <Button variant={buttonVariant}>
//             {buttonText}
//           </Button>
//       </Col>
//     </Row>
//   );
// };


// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (
    <Row>
        <Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>
            {/*<Link to={destination}>{label}</Link>*/}
            <Button variant="primary" as={Link} to={destination} >{label}</Button>

        </Col>
    </Row>
    )
}


