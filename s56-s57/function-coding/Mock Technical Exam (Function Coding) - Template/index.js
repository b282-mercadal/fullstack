function countLetter(letter, sentence) {
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    

    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined; // Return undefined if the letter is not a single character.
    }

    let result = 0;
    const letterToSearch = letter.toLowerCase(); // Convert the letter to lowercase for case-insensitive comparison.

    for (let i = 0; i < sentence.length; i++) {
        const currentLetter = sentence[i].toLowerCase(); // Convert the current letter in the sentence to lowercase.

        if (currentLetter === letterToSearch) {
            result++; // Increment the count if the letter matches the letter to search for.
        }
    }

    return result;

}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    // Convert the text to lowercase to perform a case-insensitive comparison.
    const lowercasedText = text.toLowerCase();

    // Create an object to keep track of seen letters.
    const seenLetters = {};

    // Iterate through each character in the text.
    for (let i = 0; i < lowercasedText.length; i++) {
        const letter = lowercasedText[i];

        // Check if the letter is already seen. If it is, it's not an isogram.
        if (seenLetters[letter]) {
            return false;
        }

        // Mark the letter as seen by adding it to the seenLetters object.
        seenLetters[letter] = true;
    }

    // If no repeating letters are found, it's an isogram.
    return true;
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    if (age < 13) {
        return undefined;
    }

    // Return the discounted price (rounded off) for students aged 13 to 21.
    if (age >= 13 && age <= 21) {
        const studentDiscountedPrice = Math.round(price * 0.8);
        return studentDiscountedPrice.toString();
    }

    // Return the discounted price (rounded off) for senior citizens (age 65 and above).
    if (age >= 65) {
        const seniorDiscountedPrice = Math.round(price * 0.8);
        return seniorDiscountedPrice.toString();
    }

    // Return the rounded off price for people aged 22 to 64.
    if (age >= 22 && age <= 64) {
        const roundedPrice = Math.round(price);
        return roundedPrice.toString();
    }

}

console.log(purchase(10, 100)); // Output: undefined
console.log(purchase(18, 200)); // Output: 160
console.log(purchase(35, 50.99)); // Output: 51
console.log(purchase(70, 75.25)); // Output: 60

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
        // Use a Set to store the voters who voted for candidate A.
    const votersSetA = new Set(candidateA);

    // Use a Set to store the common voters who voted for both candidate A and candidate B.
    const commonVoters = candidateB.filter((voter) => votersSetA.has(voter));

    return commonVoters;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const flyingVoters = findFlyingVoters(candidateA, candidateB);
console.log(flyingVoters);

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};